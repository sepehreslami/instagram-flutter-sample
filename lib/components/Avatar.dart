import 'package:flutter/material.dart';

class Avatar extends StatelessWidget {

  final image;
  final bool haveStory;
  final double radius;
  Avatar({this.image,this.haveStory,this.radius:15});

  @override
  Widget build(BuildContext context) {

    Color _storyColorBorder = haveStory?Colors.pink:Colors.grey[300];

    return Container(
        child: Container(
          child: CircleAvatar(
            radius: radius,
            backgroundColor: Colors.grey[300],
            backgroundImage: image,
          ),
          decoration: BoxDecoration(
            border: Border.all(width: 3,color: Colors.transparent),
            shape: BoxShape.circle
          ),
        ),
          decoration: BoxDecoration(
            border: Border.all(width: 2,color: _storyColorBorder),
            shape: BoxShape.circle
          )
    );
  }
}
