import 'package:flutter/material.dart';

import 'Avatar.dart';

class StoryAvatar extends StatelessWidget {

  final image;
  final String name;
  final bool seen;
  StoryAvatar({this.image,this.name,this.seen});

  Color _storyColorBorder;

  @override
  Widget build(BuildContext context) {

    _storyColorBorder = !seen?Colors.pink:Colors.grey[300];

    return Container(
      padding: EdgeInsets.all(10),
      color: Colors.white,
      child: Column(
//        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Avatar(
            image: image,
            radius: 30,
            haveStory: seen,
          ),
          SizedBox(height: 7),
          Text(name)
        ],
      ),
    );
  }
}
