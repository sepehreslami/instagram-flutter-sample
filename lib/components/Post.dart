import 'package:flutter/material.dart';
import 'package:instagram/components/Avatar.dart';

class PostCard extends StatefulWidget {

  final image;
  PostCard({this.image});

  @override
  _PostCardState createState() => _PostCardState(image: image);
}

class _PostCardState extends State<PostCard> {

  final image;
  _PostCardState({this.image});

  bool _bookMarkIconState = false;
  IconData _bookMarkIcon = Icons.bookmark_border;

  bool _likeState = false;
  IconData _likeIcon = Icons.favorite_border;
  Color _likeColor;

  void _setBookMark(){
    setState(() {
      _bookMarkIconState = !_bookMarkIconState;
      _bookMarkIcon = _bookMarkIconState?Icons.bookmark:Icons.bookmark_border;
    });
  }

  void _setlike(){
    setState(() {
      _likeState = !_likeState;
      _likeIcon  = _likeState?Icons.favorite:Icons.favorite_border;
      _likeColor = _likeState?Colors.red[800]:null;

    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
//        margin: EdgeInsets.all(8),
//        padding: EdgeInsets.all(16),
//        height: 250,
//        color: Colors.pink,
      child: Column(
//          mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Divider(height: 10,),
          ListTile(
            leading: Avatar(haveStory: true,image: widget.image,radius: 20,),
            title: Text('john_wick',style: TextStyle(fontWeight: FontWeight.bold),),
            subtitle: Text('London, UK',style: TextStyle(),),
            trailing: IconButton(icon: Icon(Icons.more_horiz),onPressed: (){},),
          ),
          Image(image: widget.image,),
          Container(
            height: 50,
            child: Row(
//                crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    IconButton(icon: Icon(_likeIcon,size: 35,color: _likeColor,),onPressed: _setlike,),
                    IconButton(icon: Icon(Icons.send,size: 35,),onPressed: (){},),
                  ],
                ),
                IconButton(icon: Icon(_bookMarkIcon,size: 35,),onPressed: _setBookMark,),
              ],
            ),
          )
        ],
      ),
    );
  }
}
