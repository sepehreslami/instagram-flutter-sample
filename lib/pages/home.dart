import 'package:flutter/material.dart';
import 'package:instagram/components/Post.dart';
import 'package:instagram/components/Story.dart';

class HomePage extends StatefulWidget {

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    List<StoryAvatar> _stories = [];
    List<String> _names = [
      'rock',
      'ali.123aa',
      'hassan.m.c',
      'majid_12',
      'sepehreslami',
      'aaqw123',
      'ewsa12',
      '134'
    ];
    List<AssetImage> _images = [
      AssetImage('assets/pic.jpg')
    ];
    List<bool> _seens = [
      false,
      true,
      true,
      false,
      false,
      true,
      false,
      true
    ];

    _stories.add(
        StoryAvatar(
          image: AssetImage('assets/pic.jpg')
          ,
          name: 'You',
          seen: true,
        )
    );

      for(int i=0;i<_names.length;i++){
        _stories.add(StoryAvatar(
          image: _images[0],
          name: _names[i],
          seen: _seens[i],
        ));
      }

      var _container = PostCard(image: _images[0],);

      return Padding(
        padding: const EdgeInsets.all(00.0),
        child: ListView( // parent ListView
          children: <Widget>[
            Container(
              height: 115, // give it a fixed height constraint
//              color: Colors.teal,
              // child ListView
              child: ListView.builder(
                itemCount: _stories.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (_, i) => _stories[i],

              ),
            ),
_container,
_container,
_container,
_container,
_container,
_container,
_container,
_container,
_container,
_container,
_container,
_container,
_container,
_container,
_container,
_container,
_container,
_container,
_container,

          ],
        ),
    );
    }
}

