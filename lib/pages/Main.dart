import 'package:flutter/material.dart';
import 'package:instagram/pages/home.dart';

class Main extends StatefulWidget {
  @override
  _MainState createState() => _MainState();
}

class _MainState extends State<Main> {

  int _SelectedTabIndex = 0;
  Widget _pageContent = HomePage();

  void _changeTab(int index){
    setState(() {
      _SelectedTabIndex = index;
      switch(index){
        case 0:
          _pageContent = HomePage();
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    
//    print(_stories);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0.8,
        title: Text('instagram',style:
          TextStyle(
            fontFamily: 'Billabong',
            fontSize: 37.0,
          ),
        ),
        centerTitle: true,
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
            child: IconButton(
              icon: Icon(Icons.send),
              onPressed: (){},
            ),
          )
        ],
        leading: Padding(
          padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
          child: IconButton(
            icon: Icon(Icons.camera_alt),
            onPressed: (){},
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _SelectedTabIndex,
        unselectedItemColor: Colors.grey[200],
        selectedItemColor: Colors.grey[900],
        iconSize: 30,
        onTap: _changeTab,
        items: const <BottomNavigationBarItem>[

          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('')
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            title: Text('')
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.queue),
            title: Text('')
          ),BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            title: Text('')
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text('')
          ),

        ],
      ),

      body:_pageContent
      );
  }
}
