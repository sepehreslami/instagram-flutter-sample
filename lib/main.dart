import 'package:flutter/material.dart';
import 'package:instagram/pages/Main.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Instagram',
      theme: ThemeData(
        primaryColor: Colors.white,
        accentColor: Colors.white
      ),
      home:Main()
    );
  }
}